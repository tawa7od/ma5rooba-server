import io from 'socket.io-client'
import Vue from 'vue'

const host = process.env.HOST || '127.0.0.1'
const port = process.env.PORT || 3000

Vue.prototype.$socket = io(host + ':' + port, {
  transports: ['websocket']
})
