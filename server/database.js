'use strict'

const consola = require('consola')

class Database {
  constructor(config) {
    this.host = config.host
    this.user = config.user

    this.password = config.password
    this.database = config.database

    this.knex = require('knex')({
      client: 'mysql',
      connection: {
        host: this.host,
        user: this.user,
        password: this.password,
        database: this.database
      }
    })

    this.testConnection()
      .then(() => {
        consola.success('Connected to the database.')
      })
      .catch(err => {
        consola.error({
          message: err,
          badge: true
        })
      })
  }

  async testConnection() {
    return await this.knex.raw('select 1+1 as result')
  }
}

module.exports = Database
