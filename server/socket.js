'use strict'

class Socket {
  constructor(io) {
    this.io = io
    this.initializeEventHandlers()
  }

  initializeEventHandlers() {
    this.io.on('connection', socket => {})
  }
}

module.exports = Socket
