# Ma5rooba*
**WIP*

## Mission
Ma5rooba aims to be a free secure open-source social media outlet in Egypt.

## Objectives

 - Anonymity
 - Security
 - Freedom of speech
 - Privacy

## Stacks
*Stylus*

**Node**
*Nuxt.js framework for SSR*
*Socket.io*
*Knex.js*
**MariaDB**
